import math
import numpy as np
from tabulate import tabulate
from scipy.stats import f
import matplotlib.pyplot as plt
import numpy as np

def av(x):
    return sum(x)/len(x)

def linear_model(x, y):
    n = len(x)
    b1, sn, sd = 0, 0, 0
    for xi, yi in zip(x, y):
        sn = sn + xi*yi
        sd = sd + xi**2
    b1 = (sn - n*av(x)*av(y))/(sd - n*(av(x)**2))
    b0 = av(y) - b1*av(x)
    return (b0, b1)

def SQ(x, y, b0, b1):
    SQT = SQreg = SQres = 0
    avy, avx = av(y), av(x)

    for yi in y:
        SQT = SQT + (yi - avy)**2

    for yi, xi in zip(y, x):
        SQres = SQres + (yi - b0 - b1*xi)**2

    for xi in x:
        SQreg = SQreg + (xi - avx)**2
    SQreg = SQreg * (b1**2)

    return SQreg, SQres, SQT

def QM(SQreg, SQres, SQT, n):
    QMT = SQT / (n - 1)
    QMres = SQres / (n - 2)
    QMreg = SQreg
    return QMreg, QMres, QMT

def R2(SQreg, SQT):
    return SQreg / SQT

def stat_test(QMreg, QMres):
    return QMreg / QMres

def full_analysis(x, y, pred = [], graph = False):
    b0, b1 = linear_model(x, y)
    print("The values of b0 and b1 for this data set are: {:.3f}, {:.3f}".format(b0, b1))
    print("Our linear model is represented by y = {:.3f} + {:.3f} * x".format(b0, b1))
    print('\n')
    SQreg, SQres, SQT = SQ(x, y, b0, b1)
    print("The sums of squares are: SQreg = {:.3f}, SQres = {:.3f}, SQT = {:.3f}".format(SQreg, SQres, SQT))
    QMreg, QMres, QMT = QM(SQreg, SQres, SQT, len(x))
    print("The average squares are: QMreg = {:.3f}, QMres = {:.3f}, QMT = {:.3f}".format(QMreg, QMres, QMT))
    print('\n')
    F = stat_test(QMreg, QMres)
    print("The test stat uses F distribution with 1 and {} degrees of freedom, and has the value of {:.3f}".format(len(x) - 2, F))
    print("p-value for this stat (right tail value) is: {:.3f}".format(1 - f.cdf(F, 1, len(x) - 2)))
    r2 = R2(SQreg, SQT)
    print("Coeficiente de determinacao {:.3f}".format(r2))
    print("\n")
    print("Quadro de ANOVA:");
    print(tabulate([['Regressao', '1', '{:.3f}'.format(SQreg), '{:.3f}'.format(QMreg),  '{:.3f}'.format(F) ], ['Residual', '{}'.format(len(x)-2), '{:.3f}'.format(SQres), '{:.3f}'.format(QMres)], ['Total', '{}'.format(len(x)-1), '{:.3f}'.format(SQT)]], headers=['Fonte de Variacao', 'GL', 'SQ', 'QM', 'F']))
    if len(pred) != 0:
        print('\n')
        for i in pred:
            yi = b0 + b1*i
            print("Prediction for x = {} is {:.3f}".format(i, yi))
    if graph:
        x, y = np.array(x), np.array(y)
        plt.scatter(x, y)
        plt.plot(x, b1*x + b0)
        plt.show()

x = [-260.5,-255.7,-264.6,-265,-270, -272,-272.5,-272.6,-272.8,-272.9]
y = [0.425,0.224,0.453,0.475,0.705,0.86,0.935,0.961,0.979,0.99]

graph = False

full_analysis(x, y, pred=[65], graph=graph)
