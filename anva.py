from scipy.stats import f
from tabulate import tabulate

def av(y):
    return sum(y)/len(y)

def av2(y):
    total = count = 0
    for i in y:
        for j in i:
            count = count + 1
            total = total + j
    return total / count

def SQ(y):
    SQT = SQres = SQtrat = 0
    avy = av2(y)
    avyis = [av(x) for x in y]

    for i in y:
        for j in i:
            SQT = SQT + (j - avy)**2

    for i, idx in zip(y, range(len(y))):
        for j in i:
            SQres = SQres + (j - avyis[idx])**2

    for i, idx in zip(y, range(len(y))):
        SQtrat = SQtrat + (avyis[idx] - avy)**2
    SQtrat = SQtrat*len(y[0])

    return SQT, SQtrat, SQres

def QM(SQtrat, SQres, k, r):
    QMtrat, QMres = SQtrat/(k - 1), SQres/(r - k)
    return QMtrat, QMres

def F(QMtrat, QMres):
    return QMtrat/QMres

def full_analysis(y):
    k, r = len(y), len(y[0])
    SQT, SQtrat, SQres = SQ(y)
    print("Values for SQT: {:.3f}, SQtrat: {:.3f}, SQres: {:.3f}".format(SQT, SQtrat, SQres))
    QMtrat, QMres = QM(SQtrat, SQres, k, k*r)
    print("Values for QMtrat: {:.3f}, QMres: {:.3f}".format(QMtrat, QMres))
    Fstat = F(QMtrat, QMres)
    print('\n')
    print("Our test statistic is F = {:.3f}, with {} and {} degrees.".format(Fstat, k - 1, k*(r - 1)))
    print("p-value for this stat (right tail value) is: {:.3f}".format(1 - f.cdf(Fstat, k - 1, k*(r - 1))))
    print('\n')
    print('Quadro de ANOVA')
    print(tabulate([['Tratamento', '{:.3f}'.format(k - 1), '{:.3f}'.format(SQtrat), '{:.3f}'.format(QMtrat), '{:.3f}'.format(Fstat)],
                    ['Residual', '{}'.format(k*(r - 1)), '{:.3f}'.format(SQres), '{:.3f}'.format(QMres)]],
                    headers=[' ', 'GL', 'SQ', 'QM', 'F']))


sample=[[25.6, 24.3, 27.9],
        [25.2, 28.6, 24.7],
        [20.8, 26.7, 22.2],
        [31.6, 29.8, 34.3]]

full_analysis(sample)
